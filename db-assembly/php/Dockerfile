ARG PHP_VARIANT
ARG ALPINE_VARIANT

FROM php:${PHP_VARIANT}-alpine${ALPINE_VARIANT}

LABEL maintainer="Quentin Rodriguez"

ARG XDEBUG_VARIANT
ARG POWER_SHELL_VARIANT

# Install requirements
RUN apk -X https://dl-cdn.alpinelinux.org/alpine/edge/main \ 
    add --no-cache lttng-ust

RUN apk add --no-cache \
        bash \
        openssl \
        curl \
        ca-certificates \
        less \
        ncurses-terminfo-base \
        krb5-libs \
        libgcc \
        libintl \
        libssl1.0 \
        libstdc++ \
        tzdata \
        userspace-rcu \
        zlib \
        icu-libs \
        icu-dev

# Installation des extensions php and xdebug
RUN apk add --no-cache --virtual .build-deps ${PHPIZE_DEPS} \
    && docker-php-source extract \
    && pecl channel-update pecl.php.net \
    && pecl install xdebug-${XDEBUG_VARIANT} \
    && docker-php-ext-install pdo pdo_mysql mysqli intl \
    && docker-php-ext-enable xdebug \
    && apk del -f .build-deps

# Install PowerShell
RUN mkdir -p /opt/microsoft/powershell \ 
    && curl -L https://github.com/PowerShell/PowerShell/releases/download/v${POWER_SHELL_VARIANT}/powershell-${POWER_SHELL_VARIANT}-linux-alpine-x64.tar.gz \
            -o /opt/microsoft/powershell/powershell.tar.gz \
    && tar zxf /opt/microsoft/powershell/powershell.tar.gz -C /opt/microsoft/powershell \
    && chmod +x /opt/microsoft/powershell/pwsh \
    && ln -s /opt/microsoft/powershell/pwsh /usr/bin/pwsh \
    && rm -f /opt/microsoft/powershell/powershell.tar.gz

# Copy config files for xdebug 
COPY conf.d/error_reporting.ini ${PHP_INI_DIR}/conf.d/error_reporting.ini
COPY conf.d/xdebug.ini ${PHP_INI_DIR}/conf.d/docker-php-ext-xdebug.ini
